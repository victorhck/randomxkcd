# randomxkcd

![xkcd_logo](xkcd.png)

Script de Bash para descargar un comic aleatorio de [xkcd](https://xkcd.com/) y mostrarlo en tu terminal, junto con el texto _alt_ que acompaña la imagen.

### Dependencias
* curl
* wget
* feh

Descarga el archivo 
```
https://codeberg.org/victorhck/randomxkcd/raw/branch/master/randomxkcd
```
Dale permisos de ejecución 
```
chmod +x randomxkcd
```
Ejecuta... 
```
./randomxkcd
```
et voila!

¡Descarga, prueba, disfruta, fork y mejora!

---

Bash script to download a random [xkcd](https://xkcd.com/) comic and show in your terminal also showing the _alt_ text

Download, test, enjoy, fork and improve!
